import React, { Component } from "react";
import "./App.scss";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Home } from "./Home/Home";
import { RandomNumberPage } from "./randomNumber/RandomNumberPage";
import { PersonPage } from "./Person/PersonPage";
import { TodoPage } from "./Todo/TodoPage";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <>
            <Home />
            <Route path="/randomNumber/:version" component={RandomNumberPage} />
            <Route path="/randomNumber" exact component={RandomNumberPage} />

            <Route path="/person/:version" component={PersonPage} />
            <Route path="/person" exact component={PersonPage} />

            <Route path="/todo" exact component={TodoPage} />
          </>
        </Router>
      </div>
    );
  }
}

export default App;
