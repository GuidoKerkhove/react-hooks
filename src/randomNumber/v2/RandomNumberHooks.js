import React, { useState, useEffect } from "react";
import Axios from "axios";

export function RandomNumber() {
  const [randomNumber, setRandomNumber] = useState(-1);
  const [clicked, setClicked] = useState(-1);

  useComponentDidMount(setRandomNumber);

  useEffect(() => {
    document.addEventListener("click", handleClick);
    return () => document.removeEventListener("click", handleClick);
  }, []);

  function handleClick(event) {
    console.log(`The text "${event.target.textContent} has been clicked`);
    setClicked(event.target.textContent);
  }

  return (
    <div>
      <p>
        Our clicked element: <b>{clicked}</b>
      </p>
      <p>Our random number: {randomNumber}</p>
      <br />
      <button
        onClick={async () => setRandomNumber(await loadNewRandomNumber())}
      >
        Generate new
      </button>
    </div>
  );
}

async function loadNewRandomNumber() {
  const data = await Axios.get("/randomNumber");
  return data.data;
}

function useComponentDidMount(setRandomNumber) {
  useEffect(async () => {
    const newRandomNumber = await loadNewRandomNumber();
    setRandomNumber(newRandomNumber);
  }, []);
}
