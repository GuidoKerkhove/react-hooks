import React from "react";
import Axios from "axios";

export class RandomNumber extends React.Component {
  state = {
    randomNumber: -1,
    clicked: ""
  };

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  async componentDidMount() {
    await this.loadNewRandomNumber();
    document.addEventListener("click", this.handleClick);
  }

  handleClick(event) {
    console.log(`The text "${event.target.textContent} has been clicked`);
    this.setState({ clicked: event.target.textContent });
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleClick);
  }

  render() {
    return (
      <div>
        <p>
          Our clicked element: <b>{this.state.clicked}</b>
        </p>
        <p>Our random number: {this.state.randomNumber}</p>
        <br />
        <button onClick={async () => await this.loadNewRandomNumber()}>
          Generate new
        </button>
      </div>
    );
  }

  async loadNewRandomNumber() {
    const data = await Axios.get("/randomNumber");
    this.setState({ randomNumber: data.data });
  }
}
