import React from "react";
import { Link } from "react-router-dom";

export function RandomNumberHeader() {
  return (
    <div className="header">
      <nav>
        <ul>
          <li className="header-item">
            <Link className="header-link" to="/randomNumber/1">
              V1
            </Link>
          </li>
          <li className="header-item">
            <Link className="header-link" to="/randomNumber/2">
              V2
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}
