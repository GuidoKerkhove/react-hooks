import { RandomNumber as RandomNumberHooks } from "./RandomNumberHooks";
import { RandomNumber } from "./RandomNumber";
import React from "react";

export function RandomNumber1() {
  return (
    <div>
      <h3>Versie 1</h3>
      <div className="left">
        <h2>Normale class based component</h2>
        <RandomNumber />
      </div>
      <div className="right">
        <h2>Hooks component</h2>
        <RandomNumberHooks />
      </div>
    </div>
  );
}
