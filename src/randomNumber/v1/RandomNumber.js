import React from 'react';
import Axios from 'axios';

export class RandomNumber extends React.Component {

    state = {
        randomNumber: -1,
    }

    async componentDidMount(){
        await this.loadNewRandomNumber();
    }

    render(){
        return (
            <div>
                <p>Our random number: {this.state.randomNumber}</p>
                <br/>
                <button onClick={async ()=>await this.loadNewRandomNumber()}>Generate new</button>
            </div>
        )
    }

    async loadNewRandomNumber(){
        const data = await Axios.get('/randomNumber');
        this.setState({randomNumber: data.data});
    }
}