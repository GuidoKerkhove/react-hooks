import React, { useState, useEffect } from 'react';
import Axios from 'axios';

export function RandomNumber(props) {
    const [randomNumber, setRandomNumber] = useState(-1);
    useEffect(async () => {
        const newRandomNumber = await loadNewRandomNumber();
        setRandomNumber(newRandomNumber);
    });

    return (
        <div>
            <p>Our random number: {randomNumber}</p>
            <br />
            <button onClick={async () => setRandomNumber(await loadNewRandomNumber())}>Generate new</button>
        </div>
    );
}

async function loadNewRandomNumber() {
    const data = await Axios.get('/randomNumber');
    return data.data;
}
