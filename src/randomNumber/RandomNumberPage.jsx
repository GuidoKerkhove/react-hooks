import React from "react";
import { RandomNumber1 } from "./v1/RandomNumber1";
import { RandomNumber2 } from "./v2/RandomNumber2";
import { RandomNumberHeader } from "./RandomNumberHeader";

export function RandomNumberPage(props) {
  return (
    <>
      <RandomNumberHeader />
      <Render version={props.match.params.version} />
    </>
  );
}

function Render({ version }) {
  if (version == 1) {
    return <RandomNumber1 />;
  } else if (version == 2) {
    return <RandomNumber2 />;
  } else {
    return null;
  }
}
