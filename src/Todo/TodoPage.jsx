import React from "react";
import { Todo } from "./classBased/Todo";
import { TodoHooks } from "./hooksBased/TodoHooks";
import "./todo.scss";

export function TodoPage() {
  return (
    <div>
      <h3>Todo</h3>
      <div className="left">
        <h2>Normale class based component</h2>
        <Todo />
      </div>
      <div className="right">
        <h2>Hooks component</h2>
        <TodoHooks />
      </div>
    </div>
  );
}
