import { TodoHeader } from "./TodoHeader";
import { TodoList } from "./TodoList";
import { TodoForm } from "./TodoForm";
import React from "react";

export class Todo extends React.Component {
  constructor(props) {
    super(props);
    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.markTodoDone = this.markTodoDone.bind(this);
    const todoItems = [
      { index: 1, value: "Learn react hooks basics", done: false },
      { index: 2, value: "See custom hooks", done: false },
      { index: 3, value: "See useEffect hook", done: false }
    ];
    this.state = {
      todoItems: todoItems,
      formValue: "",
      currentIndex: todoItems.length
    };
  }
  addItem() {
    this.setState({ currentIndex: this.state.currentIndex + 1 });
    this.setState({
      todoItems: [
        ...this.state.todoItems,
        {
          index: this.state.currentIndex,
          value: this.state.formValue,
          done: false
        }
      ],
      formValue: ""
    });
  }
  removeItem(itemIndex) {
    const todoItems = [...this.state.todoItems];
    todoItems.splice(itemIndex, 1);
    this.setState({ todoItems: todoItems });
  }
  markTodoDone(itemIndex) {
    const todoItems = [...this.state.todoItems];
    var todo = todoItems[itemIndex];
    todoItems.splice(itemIndex, 1);
    todo.done = !todo.done;
    todo.done ? todoItems.push(todo) : todoItems.unshift(todo);
    this.setState({ todoItems: todoItems });
  }
  render() {
    return (
      <div id="main">
        <TodoHeader />
        <TodoList
          items={this.state.todoItems}
          removeItem={this.removeItem}
          markTodoDone={this.markTodoDone}
        />
        <TodoForm
          addItem={this.addItem}
          value={this.state.formValue}
          onChange={event => this.setState({ formValue: event.target.value })}
        />
      </div>
    );
  }
}
