import React from "react";

export class TodoForm extends React.Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(event) {
    event.preventDefault();
    this.props.addItem();
  }
  render() {
    return (
      <form ref="form" onSubmit={this.onSubmit} className="form-inline">
        <input
          type="text"
          className="form-control"
          placeholder="add a new todo..."
          value={this.props.value}
          onChange={this.props.onChange}
        />
        <button type="submit" className="btn btn-default">
          Add
        </button>
      </form>
    );
  }
}
