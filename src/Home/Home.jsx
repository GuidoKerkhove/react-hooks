import React from "react";
import { Link } from "react-router-dom";

export function Home() {
  return (
    <div className="header">
      <nav>
        <ul>
          <li className="header-item">
            <Link className="header-link" to="/">
              Home
            </Link>
          </li>
          <li className="header-item">
            <Link className="header-link" to="/randomNumber">
              Random number
            </Link>
          </li>
          <li className="header-item">
            <Link className="header-link" to="/person">
              Person
            </Link>
          </li>
          <li className="header-item">
            <Link className="header-link" to="/todo">
              Todo
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}
