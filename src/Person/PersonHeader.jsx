import React from "react";
import { Link } from "react-router-dom";

export function PersonHeader() {
  return (
    <div className="header">
      <nav>
        <ul>
          <li className="header-item">
            <Link className="header-link" to="/person/1">
              V1
            </Link>
          </li>
          <li className="header-item">
            <Link className="header-link" to="/person/2">
              V2
            </Link>
          </li>
          <li className="header-item">
            <Link className="header-link" to="/person/3">
              V3
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}
