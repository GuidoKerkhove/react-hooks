import React from "react";
import { Person1 } from "./v1/Person1";
import { PersonHeader } from "./PersonHeader";

export function PersonPage(props) {
  return (
    <div className="person">
      <PersonHeader />
      <Render version={props.match.params.version} />
    </div>
  );
}

function Render({ version }) {
  if (version == 1) {
    return <Person1 />;
  } else if (version == 2) {
    // return <RandomNumber2 />;
  } else {
    return null;
  }
}
