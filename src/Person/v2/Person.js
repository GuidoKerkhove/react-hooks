import React, { useState } from 'react';

export function Person() {
    const [firstName, onChangeFirstName] = useOnChange('Guido');
    const [lastName, onChangeLastName] = useOnChange('Kerkhove');

    return (
        <div>
            <label>First name:</label>
            <input onChange={onChangeFirstName} value={firstName}></input>
            <label>Last name:</label>
            <input onChange={onChangeLastName} value={lastName}></input>
        </div>
    )
}

function useOnChange(initialValue) {
    const [value, set] = useState(initialValue);
    const onChange = function (event) {
        set(event.target.value);
    }

    return [value, onChange];
}