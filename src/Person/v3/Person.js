import React, { useState } from 'react';

export function Person() {

    const [person, onChangePerson] = useOnChangeObject({ firstName: 'Guido', lastName: 'Kerkhove' })

    return (
        <div>
            <label>First name:</label>
            <input onChange={(event) => onChangePerson(event, 'firstName')} value={person.firstName}></input>
            <label>Last name:</label>
            <input onChange={(event) => onChangePerson(event, 'lastName')} value={person.lastName}></input>
        </div>
    )
}

function useOnChangeObject(initialValue) {
    const [object, set] = useState(initialValue);
    const onChange = function (event, key) {
        set({ ...object, [key]: event.target.value })
    }

    return [object, onChange];

}

