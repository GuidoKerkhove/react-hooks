import React from "react";
import { Person } from "./Person";
import { Person as PersonHooks } from "./PersonHooks";

export function Person1() {
  return (
    <div>
      <h3>Versie 1</h3>
      <div className="left">
        <h2>Normale class based component</h2>
        <Person />
      </div>
      <div className="right">
        <h2>Hooks component</h2>
        <PersonHooks />
      </div>
    </div>
  );
}
