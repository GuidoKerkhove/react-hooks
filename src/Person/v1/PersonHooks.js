import React, { useState } from "react";

export function Person() {
  const [firstName, setFirstName] = useState("Guido");
  const [lastName, setLastName] = useState("Kerkhove");

  function onChangeFirstName(event) {
    const value = event.target.value;
    setFirstName(value);
  }
  function onChangeLastName(event) {
    const value = event.target.value;
    setLastName(value);
  }

  return (
    <div>
      <div>
        <label>First name:</label>
        <input onChange={onChangeFirstName} value={firstName} />
      </div>
      <div>
        <label>Last name:</label>
        <input onChange={onChangeLastName} value={lastName} />
      </div>
    </div>
  );
}
