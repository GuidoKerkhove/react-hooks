import React from "react";

export class Person extends React.Component {
  state = {
    firstName: "Guido",
    lastName: "Kerkhove"
  };

  onChangeFirstName(event) {
    const value = event.target.value;
    this.setState({ firstName: value });
  }

  onChangeLastName(event) {
    const value = event.target.value;
    this.setState({ lastName: value });
  }

  render() {
    return (
      <div>
        <div>
          <label>First name:</label>
          <input
            onChange={event => this.onChangeFirstName(event)}
            value={this.state.firstName}
          />
        </div>
        <div>
          <label>Last name:</label>
          <input
            onChange={event => this.onChangeLastName(event)}
            value={this.state.lastName}
          />
        </div>
      </div>
    );
  }
}
